<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.service.IMetierService"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Ajouter une personne</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Ajouter une personne</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="index.html"><i
					class="fas fa-home"></i> Accueil <span class="sr-only">(current)</span></a>
				<a class="nav-item nav-link" href="add2.do"><i
					class="fas fa-plus"></i> Ajouter un métier</a>
			</div>
		</div>
	</nav>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col card col-lg-12">
				<form action="add.do" method="post">
					<div class="form-group">
						<label for="nom">Nom :</label> 
						<input type="text" class="form-control" name="nom" id="nom" required placeholder="Catteau">
					</div>
					<div class="form-group">
						<label for="prenom">Prénom :</label> 
						<input type="text" class="form-control" name="prenom" id="prenom" required placeholder="Jean-Philippe">
					</div>
					<div class="form-group">
						<label for="adresse">Adresse :</label> 
						<input type="text" class="form-control" name="adresse" id="adresse" required placeholder="20 Rue du Luxembourg">
					</div>
					<div class="form-group">
						<label for="dateNaissance">Date de naissance :</label> 
						<input type="date" class="form-control" name="dateNaissance" id="dateNaissance" required placeholder="12/12/1996">
					</div>
					<div class="form-group">
						<label for="metier">Métier :</label> 
						<select name="metier" id="metier" required>
						<%
							List<MetierDto> listeMetier = (List<MetierDto>) request.getAttribute("listMetier");
							for(MetierDto m : listeMetier) {
						%>
							<option value="<%=m.getId()%>"><%=m.getLabel()%></option>
						<%
							}
						%>
						</select>
					</div>
					<button type="submit" class="btn btn-primary btn-dark">Ajouter
						l'utilisateur !</button>
				</form>
				<a class="btn btn-secondary btn-dark" href="list.do" role="button">retour
					vers la liste</a>
			</div>
		</div>
	</div>
	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>