<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Show</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Informations personnelles</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="index.html"><i
					class="fas fa-home"></i> Accueil <span class="sr-only">(current)</span></a>
				<a class="nav-item nav-link" href="add.do"><i
					class="fas fa-plus"></i> Ajouter un utilisateur</a> <a
					class="nav-item nav-link" href="add2.do"><i class="fas fa-plus"></i>
					Ajouter un métier</a>
			</div>
		</div>
	</nav>
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col card col-lg-12">
				<form>
					<div class="form-group row">
						<label for="idPers" class="col-sm-2 col-form-label">Identifiant</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="idPers" value="${personne.id }">
						</div>
					</div>
					<div class="form-group row">
						<label for="nomPers" class="col-sm-2 col-form-label">Nom</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="nomPers" value="${personne.nom }">
						</div>
					</div>
					<div class="form-group row">
						<label for="prenomPers" class="col-sm-2 col-form-label">Prénom</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="prenomPers" value="${personne.prenom }">
						</div>
					</div>
					<div class="form-group row">
						<label for="adressePers" class="col-sm-2 col-form-label">Adresse</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="adressePers" value="${personne.adresse }">
						</div>
					</div>
					<div class="form-group row">
						<label for="dateNaissancePers" class="col-sm-2 col-form-label">Date	de naissance</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="dateNaissancePers" value="${personne.dateNaissance}">
						</div>
					</div>
					<div class="form-group row">
						<label for="metierPers" class="col-sm-2 col-form-label">Métier</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="metierPers" value="${personne.metier.label }">.
						</div>
					</div>
				</form>
				<a class="btn btn-secondary btn-dark" href="list.do" role="button">retour
					vers la liste</a>
			</div>
		</div>
	</div>
	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>