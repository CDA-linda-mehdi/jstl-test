<%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.IPersonneService"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Accueil</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="list.do">Gestion des personnes</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link" href="add.do"><i
					class="fas fa-plus"></i> Ajouter un utilisateur</a> <a
					class="nav-item nav-link" href="add2.do"><i class="fas fa-plus"></i>
					Ajouter un métier</a>
			</div>
		</div>
	</nav>

	<%
		ReponseDto reponse = (ReponseDto) request.getAttribute("reponse");
		if (reponse != null) {
			String typeMsg = reponse.getStatus() == ReponseStatut.OK ? "primary" : "danger";
			String alert = "alert-" + typeMsg;
	%>
	<div class="alert <%=alert%> alert-dismissible fade show" role="alert">
		<strong><%=reponse.getMsg()%></strong>
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<%
		}
	%>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-12">
				<table class="table table-hover">
					<thead class="thead-dark">
						<tr>
							<th>ID</th>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Corbeille</th>
						</tr>
					</thead>

					<tbody>
						<%
							IPersonneService personneS = (IPersonneService) request.getAttribute("personneService");
							int nbrPageT = 0;
							int pageR = (Integer) request.getAttribute("page");
							if (personneS != null) {
								List<PersonneDto> personne = personneS.chercherToutesLesPersonnes(pageR);
								nbrPageT = personne.size();
								for (PersonneDto p : personne) {
									String idStr = Integer.toString(p.getId());
									String show = "show.do?id=" + idStr;
									String blackout1 = "#blackout" + idStr;
									String blackout2 = "blackout" + idStr;
									String box = "box" + idStr;
									String delete = "delete.do?id=" + idStr;
						%>
						<tr id="<%=idStr%>">
							<td><a style="display: block; width: 100%; height: 100%;"
								href="<%=show%>"><%=idStr%></a></td>
							<td><a style="display: block; width: 100%; height: 100%;"
								href="<%=show%>"><%=p.getNom()%></a></td>
							<td><a style="display: block; width: 100%; height: 100%;"
								href="<%=show%>"><%=p.getPrenom()%></a></td>
							<td><a
								style="display: block; width: 100%; height: 100%; color: red;"
								href="<%=blackout1%>"><i class="fas fa-trash-alt"></i></a>
								<div class="blackout" id="<%=blackout2%>">
									<div class="box" id="<%=box%>" value="<%=idStr %>">
										<input type="hidden" id="identifiant" value="<%=idStr %>" >
										Êtes-vous sur? 
										<a id="<%="ouiClose" + idStr %>" class="a-validate-delete close" href="#">oui</a> 
										<a href="#" class="close">non</a>
									</div>
								</div></td>
						</tr>
						<%
							}
								;
							}
						%>
					</tbody>
				</table>

				<nav>
					<ul class="pagination justify-content-center">
						<%
							int moins = pageR - 1;
							int plus = pageR + 1;
							String list1 = "list.do?page=" + moins;
							String list2 = "list.do?page=" + plus;
							String listdyna = "list.do?page=";
							int nbrPage = PersonneServiceImpl.nbrPage;
							int fin = nbrPageT;
							if (pageR > 0) {
						%>
						<li><a class="page-link" href="<%=list1%>" tabindex="-1">Précédent</a>
						</li>
						<%
							} else {
						%>
						<li class="page-item disabled"><a class="page-link"
							href="<%=list1%>" tabindex="-1">Précédent</a></li>
						<%
							}
							for (int i = 0; i < nbrPage; i++) {
						%>
						<li class="page-item"><a class="page-link"
							href="<%=listdyna + i%>"><%=i+1%></a></li>
						<%
							}
						%>
						<li class="page-item">
							<%
								if (fin == 5) {
							%>
						
						<li><a class="page-link" href="<%=list2%>">Suivant</a></li>
						<%
							} else {
						%>
						<li class="page-item disabled"><a class="page-link"
							href="<%=list2%>">Suivant</a></li>
						<%
							}
						%>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="js/delete.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>