$(document).ready(function() {
	$('.a-validate-delete').on('click',function(event) {
		var idToDelete = $(this).parent().find("input[id='identifiant']").val();
		event.stopPropagation();
		
		$.post("delete.do?id="+idToDelete, {
			id : idToDelete,
		}).done(function(data, status) {
			console.log("La suppression est réussie.");
			// supprimer la ligne
			$("tr[id='"+idToDelete+"']").remove();
		});
		return false;
	});
});