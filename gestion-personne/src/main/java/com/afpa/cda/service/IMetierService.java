package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.afpa.cda.dto.MetierDto;

public interface IMetierService {
	public List<MetierDto> findAll();
	public Optional<MetierDto> trouverParLabel(@Param(value = "label")String label);
	public Optional<MetierDto> findById(int id);
	public Integer ajouterUnMetier(MetierDto mDto);
}
