package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.MetierRepository;
import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.entity.Metier;

@Service
public class MetierServiceImpl implements IMetierService {

	@Autowired
	private MetierRepository metierRepository;
	
	@Override
	public List<MetierDto> findAll() {
		List<MetierDto> maListe = this.metierRepository.findAll()
				.stream()
				.map(e->MetierDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.build())
				.collect(Collectors.toList());
		return maListe;
	}

	@Override
	public Optional<MetierDto> findById(int id) {
		Optional<Metier> met = this.metierRepository.findById(id);
		Optional<MetierDto> res = Optional.empty();
		if (met.isPresent()) {
			Metier m = met.get();
			MetierDto metDto = MetierDto.builder().id(m.getId()).label(m.getLabel()).build();
			res = Optional.of(metDto);
		}
		return res;
	}
	
	@Override
	public Optional<MetierDto> trouverParLabel(String label) {
		Optional<Metier> met = this.metierRepository.findByLabel(label);
		Optional<MetierDto> res = Optional.empty();
		if (met.isPresent()) {
			Metier m = met.get();
			res = Optional.of(MetierDto.builder().id(m.getId()).label(m.getLabel()).build());
		}
		return res;
	}

	@Override
	public Integer ajouterUnMetier(MetierDto mDto) {
		Metier m = Metier.builder().label(mDto.getLabel()).build();
		m = this.metierRepository.save(m);
		return m.getId();
	}
}
