package com.afpa.cda.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Personne {
	@Id
	@GeneratedValue(generator="personne_seq") //Cr�ation d'une s�quence pour la table personne
    private int id;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	@ManyToOne
	private Metier metier;
	private String adresse;
}
