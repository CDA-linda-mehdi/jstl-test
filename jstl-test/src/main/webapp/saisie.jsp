<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Calculatrice</title>
</head>
<body>

	<form action="cal.do" method="post">
		<input type="number" name="num1" /> <input type="number" name="num2" />
		<br /> <input name="signe" type="radio" value="+">+</input> <input
			name="signe" type="radio" value="-">-</input> <input name="signe"
			type="radio" value="*">*</input> <input name="signe" type="radio"
			value="/">/</input> <input type="submit" value="Calcul">
	</form>

	<div>
		<p style="display: inline;">resultat : ${ res }</p>
	</div>

</body>
</html>