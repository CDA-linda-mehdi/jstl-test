<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours JSTL</title>
</head>
<body>
	<h1>
		<c:out value="${ msg }" />
	</h1>

	<!-- Affichage de la liste de Personnes de manière aléatoire -->
	
			
	<table>
	
		<thead>
			<tr>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Age</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ listPers }" var="element" varStatus="status">
			<c:choose>
			<c:when test="${ status.count % 2 == 0 }">
				<tr style="background-color: red">
					<td><c:out value="${ element.nom }" />
			
					<td><c:out value="${ element.prenom }" />
					<td><c:out value="${ element.age }" />
						</tr>
						</c:when>
						<c:otherwise>
						<tr style="background-color: green">
					<td><c:out value="${ element.nom }" />
					<td><c:out value="${ element.prenom }" />
					<td><c:out value="${ element.age }" />
						</tr>
						</c:otherwise>
						</c:choose>
						
			</c:forEach>
		</tbody>
	</table>

	<p></p>
</body>
</html>