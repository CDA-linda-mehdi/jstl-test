package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/cal.do")
public class CalculServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CalculServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		this.getServletContext().getRequestDispatcher("/saisie.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String num1Param = request.getParameter("num1");
		System.out.println(num1Param);
		String num2Param = request.getParameter("num2");
		System.out.println(num2Param);
		String signeParam = request.getParameter("signe");
		System.out.println(signeParam);
		
		if (num1Param == null || num2Param == null || signeParam == null
				|| num1Param.length() == 0 || num2Param.length() == 0 || signeParam.length() == 0) {
			request.setAttribute("erreur", "Il manque un param�tre");
			this.getServletContext().getRequestDispatcher("/saisie.jsp").forward(request, response);
		} else {
			int res = 0;
			int param1;
			int param2;
			try {
				param1 = Integer.parseInt(num1Param);
				param2 = Integer.parseInt(num2Param);

				switch (signeParam) {
				case "+":
					res = param1 + param2;
					break;
				case "-":
					res = param1 - param2;
					break;
				case "*":
					res = param1 * param2;
					break;
				case "/":
					try {
						res = param1 / param2;
					} catch (Exception e) {
						request.setAttribute("erreur", "<div class=\"alert alert-danger\" role=\"alert\"> Erreur : Division par 0 impossible</div>");
						System.err.println("Pas de division par 0");
					}
					
					break;
				default:
					request.setAttribute("erreur", "");
					break;
				}
				request.setAttribute("res", res);
				
				this.getServletContext().getRequestDispatcher("/saisie.jsp").forward(request, response);
			} catch (Exception e) {
				System.out.println("erreur");
			}
		}
		
		

	}
}
