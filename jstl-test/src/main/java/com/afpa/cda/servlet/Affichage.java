package com.afpa.cda.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.afpa.cda.entity.Personne;

@WebServlet(urlPatterns = { "/aff.do" })
public class Affichage extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Personne pers1 = new Personne();
		pers1.setNom("Denis");
		pers1.setPrenom("Jean");
		pers1.setAge(37);
		request.setAttribute("personne1", pers1);

		// Cr�ation d'une servlet qui instancie un nombre al�atoire d'objets d'un type Personne
		Random r = new Random();
		int nbrAleatoire = r.nextInt(10) + 1;
		int ageP = 2;
		String nomP = "Toto";
		String prenomP = "Tutu";

		List<Personne> listPers = new ArrayList<>();
		// Les attributs des personnes auront des valeurs diff�rentes
		for (int i = 0; i < nbrAleatoire; i++) {
			Personne p = new Personne(prenomP + i, nomP + i, ageP + i);
			listPers.add(p);
		}

		// Cette servlet transf�re les objets personnes � une JSP
		request.setAttribute("listPers", listPers);
		request.setAttribute("nbrAleatoire", nbrAleatoire);
		String nextJSP = "/vue.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
	}

}
